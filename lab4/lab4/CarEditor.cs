﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab4
{
    public partial class CarEditor : Form
    {
        private AbstractCar _car;   //Храние информацию о машине, которая редактируется или создается в форме
        private DialogResult _result;   //Результат диалога, те нажатие ОК или Отмены
        public AbstractCar Car
        {
            get { return _car; }
        }
        public DialogResult Result
        {
            get { return _result; }
        }
        public CarEditor() {
            InitializeComponent();

            //Добавляем в поле класс возможные типы автомобилей
            carClass.Items.Add(typeof(PassengerCar));
            carClass.Items.Add(typeof(Lorry));
        }

        //Конструктор для редакитрования автомобилей
        public CarEditor(AbstractCar car) : this() {
            if (car != null) {
                //Заполенине полей из аргумента конструктора Car
                carClass.Text = car.GetType().ToString();
                carModel.Text = car.Model;
                carPrice.Text = car.Price.ToString();

                //Определение типа автомобиля и заполенение оставшихся полей
                if (car.GetType() == typeof(PassengerCar)) {
                    PassengerCar _car = (PassengerCar)car;
                    carSeats.Text = _car.Seats.ToString();

                } else if (car.GetType() == typeof(Lorry)) {
                    Lorry _car = (Lorry)car;
                    carSeats.Text = _car.Seats.ToString();
                    carCapacity.Text = _car.Capacity.ToString();
                }
            }
        }

        //Нажание на кропку OK, те сохранение отредактированных изменений
        private void button1_Click_1(object sender, EventArgs e) {
            int price, seats, capacity = 0;

            if (int.TryParse(carPrice.Text, out price))
            {
                MessageBox.Show("Вы ввели неверную цену!");
                return;
            }
            if (int.TryParse(carSeats.Text, out seats))
            {
                MessageBox.Show("Вы ввели неверное количество сидения!");
                return;
            }
            if (int.TryParse(carCapacity.Text, out capacity))
            {
                MessageBox.Show("Вы ввели неверную грузоподъемность!");
                return;
            }

            if (carClass.Text == "PassengerCar") {
                _car = new PassengerCar(carModel.Text, price, seats);
            } else {
                _car = new Lorry(carModel.Text, price, seats, capacity);
            }

            _result = DialogResult.OK;
            this.Close();
        }

        //Закрытие формы
        private void button2_Click(object sender, EventArgs e) {
            _result = DialogResult.Cancel;
            this.Close();
        }

        //Отключение поля carCapacity для пассажирских автомобилей
        private void carClass_SelectedIndexChanged(object sender, EventArgs e) {
            if (carClass.Text == "PassengerCar") {
                carCapacity.Enabled = false;
                label5.Enabled = false;
            } else {
                carCapacity.Enabled = true;
                label5.Enabled = true;
            }
        }
    }
}