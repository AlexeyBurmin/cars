﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab4
{
    public partial class MainWindow : Form
    {
        private CarPark park = new CarPark();
        public MainWindow() {
            InitializeComponent();
        }

        //Обновление общей цены автоболией
        public void RefreshPrice() {
            priceLabel.Text = park.GetTotalPrice().ToString() + "$";
        }

        //Генерация рандомного парка автомобилей и добавление их в listBox
        private void button1_Click(object sender, EventArgs e) { //rand
            park = CarFactory.CreatePark();
            List<AbstractCar> cars = park.GetCarList();

            carList.Items.Clear();
            foreach (AbstractCar car in cars) {
                carList.Items.Add(car.Model + " - " + car.Price.ToString() + "$");
            }
            RefreshPrice();
        }

        //Добавление автомобиля в список через вызов формы AddEditCar
        private void button2_Click(object sender, EventArgs e) { //add
            CarEditor add = new CarEditor();
            add.ShowDialog();
            if (add.Result == DialogResult.OK) {
                carList.Items.Add(add.Car.Model + " - " + add.Car.Price.ToString() + "$");
                park.AddCar(add.Car);
                RefreshPrice();
            }
            add.Dispose();
        }

        //Редактирование выделенного автомобиля
        private void button3_Click(object sender, EventArgs e) { //edit
            if (carList.SelectedItem != null) {
                CarEditor edit = new CarEditor(park.GetCarList()[carList.SelectedIndex]);
                edit.ShowDialog();
                if (edit.Result == DialogResult.OK) {
                    park.GetCarList()[carList.SelectedIndex] = edit.Car;
                    RefreshPrice();
                }
                edit.Dispose();
            }
        }

        //Удаление автомобиля из списка
        private void button4_Click(object sender, EventArgs e) { //del
            if (carList.SelectedItem != null) {
                park.RemoveCar(park.GetCarList()[carList.SelectedIndex]);
                carList.Items.RemoveAt(carList.SelectedIndex);
                RefreshPrice();
            }
        }
    }
}