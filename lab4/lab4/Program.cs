﻿using System;

namespace lab4
{
    class Program
    {
        //Основной класс для запуска программы
        static void Main(string[] args) {
            Console.WriteLine("Каким способом вы хотите запустить приложение? Если в окне, то введите 1, если в консоли введите 0!");
            string answ = Console.ReadLine();
            if (answ == "0") {
                CarPark park = CarFactory.CreatePark();

                Printer printer = new Printer();
                printer.Print(park);

                Console.ReadKey();
            } else if (answ == "1") {
                MainWindow main = new lab4.MainWindow();
                main.ShowDialog();
            }
            else {
                Console.WriteLine("Неверный символ!");
            }
        }

    }
}