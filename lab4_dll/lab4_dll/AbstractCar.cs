﻿//Базовый класс описывающий основные свойства всех машин
public class AbstractCar
{
    protected int _price;   //цена автомобиля, protected => есть доступ из классов наследников
    protected string _model;    //модель автомобиля

    public virtual int Price    //Виртуал обозначает, что функция может быть переопределена в классе наследнике
    {
        get { return _price; }
        set { _price = value; }
    }

    public string Model
    {
        get { return _model; }
        set { _model = value; }
    }
}