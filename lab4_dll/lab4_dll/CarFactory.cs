﻿using System;

//Класс для произодства автомобилей, заполнение парка CarPark
public class CarFactory
{
    //Список марок автомобилей
    private static string[] cars = {
        "Ford",
        "Nissan",
        "Renault",
        "Mazda",
        "Acura",
        "Subaru",
        "Suzuki",
        "Chevrolet",
        "Lada",
        "Porsche",
        "Kia"
    };
    public static CarPark CreatePark() {
        Random rand = new Random();
        CarPark park = new CarPark();

        int type = rand.Next(0, 1);

        //Задание рандомного парка из рандомного кол-ва автомобилей
        for (int i = 0; i < rand.Next(10, 50); i++) {
            if (type == 0)
                park.AddCar(new PassengerCar(cars[rand.Next(0, cars.Length)], rand.Next(500, 100000), rand.Next(2, 6)));
            else
                park.AddCar(new Lorry(cars[rand.Next(0, cars.Length)], rand.Next(500, 100000), rand.Next(2, 6), rand.Next(100, 000)));
        }
        return park;
    }
}