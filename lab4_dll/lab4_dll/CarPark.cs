﻿using System;
using System.Collections.Generic;


public class CarPark
{
    private List<AbstractCar> cars = new List<AbstractCar>();

    //Добавление машины в парк
    public void AddCar(AbstractCar car) {
        cars.Add(car);
    }

    //Удаление машины с парка
    public void RemoveCar(AbstractCar car) {
        cars.Remove(car);
    }

    //Получение общей цены автопарка
    public int GetTotalPrice() {
        int total_price = 0;
        foreach (AbstractCar car in cars) {
            total_price += car.Price;
        }
        return total_price;
    }

    //Получение списка машин в парке в виде массива строк (Модель, Цена$)
    public string[] GetCarListString() {
        string[] r_car = new string[cars.Count];
        for (int i = 0; i < cars.Count; i++) {
            r_car[i] = cars[i].Model + ", " + cars[i].Price.ToString() + "$";
        }
        return r_car;
    }

    //Получить массив с автомобилями
    public List<AbstractCar> GetCarList() {
        return cars;
    }
}