﻿//Грузовые автомобили
public class Lorry : PassengerCar
{
    private int _carryingCapacity;  //грузоподъемность

    public int Capacity
    {
        get { return _carryingCapacity; }
        set { _carryingCapacity = value; }
    }

    public override int Price
    {
        set { _price = value + 50 * _numberOfSeats + 2 * _carryingCapacity; }   //аналогично пассаж, только прибавляем 2 * грузоподъемность
    }
    public Lorry(string model, int price, int seats, int cap) : base(model, price, seats) {
        Model = model;
        Seats = seats;
        Capacity = cap;
        Price = price;
    }
}