﻿//Класс описывающий пассажирский автомобиль
public class PassengerCar : AbstractCar
{
    protected int _numberOfSeats;  //Количество сидений

    public int Seats
    {
        get { return _numberOfSeats; }
        set { _numberOfSeats = value; }
    }

    public override int Price
    {
        set { _price = value + 50 * _numberOfSeats; }   //переопределение сетера для цены, те цена вычисляется как заданое значение + 50 * кол-во сидений
    }

    public PassengerCar(string model, int price, int seats) {
        Model = model;
        Seats = seats;
        Price = price;
    }
}