﻿using System;
using System.Collections.Generic;

//Класс для вывода информации в консоль (Номер. Модель, Цена$)
public class Printer
{
    public void Print(CarPark park) {
        List<AbstractCar> cars = park.GetCarList();
        for (int i = 0; i < cars.Count; i++) {
            Console.WriteLine((i + 1) + ". " + cars[i].Model + ", " + cars[i].Price.ToString() + "$");
        }
        Console.WriteLine("Total price = " + park.GetTotalPrice() + "$");
    }
}
