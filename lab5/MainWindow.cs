﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace lab5
{
    public partial class MainWindow : Form
    {
        private CarPark park = new CarPark();
        public MainWindow() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {  //add car
            CarEditor add = new CarEditor();
            add.ShowDialog();
            if (add.Result == DialogResult.OK) {
                carList.Items.Add(add.Car.Model + " - " + add.Car.Price.ToString() + "$");
                park.AddCar(add.Car);
                RefreshPrice();
            }
            f2.Dispose();
        }

        private void button3_Click(object sender, EventArgs e) {  //edit car
            if (carList.SelectedItem != null) {
                CarEditor edit = new CarEditor(park.GetCarList()[carList.SelectedIndex]);
                edit.ShowDialog();
                if (edit.Result == DialogResult.OK) {
                    park.GetCarList()[carList.SelectedIndex] = edit.Car;
                    RefreshPrice();
                }
                edit.Dispose();
            }
            
        }

        private void button2_Click(object sender, EventArgs e) {  //delete car
            if (carList.SelectedItem != null) {
                park.RemoveCar(park.GetCarList()[carList.SelectedIndex]);
                carList.Items.RemoveAt(carList.SelectedIndex);
                RefreshPrice();
            }
        }

        private void RandPark_Click(object sender, EventArgs e) {
            park = CarFactory.CreatePark();
            List<AbstractCar> cars = park.GetCarList();

            carList.Items.Clear();
            foreach (AbstractCar car in cars) {
                carList.Items.Add(car.Model + " - " + car.Price.ToString() + "$");
            }
            RefreshPrice();
        }

        public void RefreshPrice() {
            priceLabel.Text = park.GetTotalPrice().ToString() + "$";
        }
    }
}
